<?php
	/*
	Plugin Name: zipStatic
	Plugin URI: http://pm8.jp
	Description: zip static pages
	Author: Yuki TANABE
	Version: 0.1
	Author URI: http://pm8.jp
	License: MIT
	*/

$zipStatic = new zipStatic();
class zipStatic{
	const CAPABILITY = 'administrator';
	const MENU_SLAG = 'zipStatic';
	const MENU_TITLE = 'ZipStatic';
	const OPTION_STATIC_PATH='zs_staticPath';
	const OPTION_ZIP_PATH='zs_zipPath';
	const OPTION_ZIP_FILENAME='zs_zipFilename';
	const PAGE_TITLE = 'Zip Static';
	const SHOW_MENU_FUNC = 'zs_showMenu';
	const SHOW_PAGE_FUNC = 'zs_showPage';

	public function __construct(){
		add_action('admin_menu', array($this, zipStatic::SHOW_MENU_FUNC), 1000);
	}
	function zs_createZip (){
		$out='<p>failed</p>';
		if(get_option(zipStatic::OPTION_STATIC_PATH)&&get_option(zipStatic::OPTION_ZIP_PATH)&&get_option(zipStatic::OPTION_ZIP_FILENAME)) {
			exec('cd '.get_option(zipStatic::OPTION_STATIC_PATH).'; rm -f '.get_option(zipStatic::OPTION_ZIP_PATH).get_option(zipStatic::OPTION_ZIP_FILENAME).'; zip -r '.get_option(zipStatic::OPTION_ZIP_PATH).get_option(zipStatic::OPTION_ZIP_FILENAME).' * ', $out);
		}
		return $out;
	}
	function zs_showMenu(){
		add_menu_page(zipStatic::PAGE_TITLE, zipStatic::MENU_TITLE, zipStatic::CAPABILITY, zipStatic::MENU_SLAG, array($this,zipStatic::SHOW_PAGE_FUNC) /*, $icon_url, $position */);
	}
	function zs_showPage(){
		$r='';
		if (isset($_POST[zipStatic::OPTION_STATIC_PATH])) {
			update_option(zipStatic::OPTION_STATIC_PATH, wp_unslash($_POST[zipStatic::OPTION_STATIC_PATH]));
		}
		if (isset($_POST[zipStatic::OPTION_ZIP_PATH])) {
			update_option(zipStatic::OPTION_ZIP_PATH, wp_unslash($_POST[zipStatic::OPTION_ZIP_PATH]));
		}
		if (isset($_POST[zipStatic::OPTION_ZIP_FILENAME])) {
			update_option(zipStatic::OPTION_ZIP_FILENAME, wp_unslash($_POST[zipStatic::OPTION_ZIP_FILENAME]));
		}
		if(isset($_POST[zipStatic::OPTION_STATIC_PATH])&&isset($_POST[zipStatic::OPTION_ZIP_PATH])&&isset($_POST[zipStatic::OPTION_ZIP_FILENAME])){
			$r = zipStatic::zs_createZip();
		}
		echo <<<EOT
<h1>Zip Static</h1>
<h2>set up</h2>
EOT;
		if (isset($_POST[zipStatic::OPTION_STATIC_PATH])) {
			echo <<<EOT
			<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">
				<p><strong>setting updated.</strong></p>
			</div>
EOT;
		}
		echo <<<EOT
<form method="post" action="">
<table class="form-table">
EOT;
		echo '<tr><th><label for="zs_path">Static Directory (HTML)</label></th>';
		echo '<td><input name="'.zipStatic::OPTION_STATIC_PATH.'" id="'.zipStatic::OPTION_STATIC_PATH.'" class="regular-text" placeholder="/path/to/your/static/site/directory/" value="'.get_option(zipStatic::OPTION_STATIC_PATH).'" /></td></tr>';
		echo '<tr><th><label for="zs_path">Save Directory (ZIP)</label></th>';
		echo '<td><input name="'.zipStatic::OPTION_ZIP_PATH.'" id="'.zipStatic::OPTION_ZIP_PATH.'" class="regular-text" placeholder="/path/to/your/ZIP-FILE/directory/" value="'.get_option(zipStatic::OPTION_ZIP_PATH).'" /></td></tr>';
		echo '<tr><th><label for="zs_path">FILE NAME (ZIP)</label></th>';
		echo '<td><input name="'.zipStatic::OPTION_ZIP_FILENAME.'" id="'.zipStatic::OPTION_ZIP_FILENAME.'" class="regular-text" placeholder="result.zip" value="'.get_option(zipStatic::OPTION_ZIP_FILENAME).'" /></td></tr>';
		echo '<table>';
		submit_button('Build ZIP File');
		echo <<<EOT
</form>
EOT;
		if($r){
			echo '<h2>result</h2><pre style="max-height:300px;overflow:scroll;">'.implode("\n",$r).'</pre>';
			echo '<p>ZIP FILE saved from ['.get_option(zipStatic::OPTION_STATIC_PATH).'].</p>';
			echo '<p>ZIP FILE saved at ['.get_option(zipStatic::OPTION_ZIP_PATH).get_option(zipStatic::OPTION_ZIP_FILENAME).'].</p>';
		}
	}
}