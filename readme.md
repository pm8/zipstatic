# zipStatic
## What is this?
zipStatic is a wordpress plugin to zip **STATIC** pages.

## how to use?
- install and enable zipStatic in your wordpress.
- access `YOUR-SITE.com/wp-admin/` and select `Zip Static` from left menu.
- set your environment:
    - `Static Directory (HTML)` to fill path to your **STATIC** site.
    - `Save Directory (ZIP)` to write directory from server root, **NEVER include file name.**
    - `FILE NAME (ZIP)` to fill file name.

*if you do not have static files, you need to generate them in another plugins or so on.*